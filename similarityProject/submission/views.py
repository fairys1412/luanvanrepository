import requests
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.generic import TemplateView, FormView, UpdateView, ListView, CreateView, DetailView
from rest_framework import viewsets, permissions, status, generics
from rest_framework.decorators import action
from rest_framework.response import Response
from django.http import HttpResponseRedirect
from django.views.generic import ListView, TemplateView
from django.urls import reverse_lazy

from .forms import *
from .models import *
# from .serializers import BanThaoSerializer, TLTKSerializer, UserSerializer
from rest_framework.parsers import MultiPartParser


# User = get_user_model()
# # Create your views here.
#
# class UserViewSet(viewsets.ViewSet,
#                   generics.CreateAPIView,
#                   generics.RetrieveAPIView):
#     # 1 API đăng ký User - 1 API lấy thông tin User
#     queryset = User.objects.filter(is_active=True)
#     serializer_class = UserSerializer
#     parser_classes = [MultiPartParser, ]
#
#     # lấy thông tin User
#     def get_permissions(self):
#         if self.action == 'retrieve':
#             return [permissions.IsAuthenticated()]
#         return [permissions.AllowAny()]
#
#
# class BanThaoViewSet(viewsets.ModelViewSet):  # kế thừa view chuẩn từ django
#     queryset = BanThao.objects.filter(active=True)
#     serializer_class = BanThaoSerializer
#     # queryset: đổ các câu truy vấn
#     # serializer_class: thể hiện các câu truy vấn ra phía client
#     # Từ 2 câu khai báo trên, ta được 5 API
#     # list (GET)--> xem ds bản thảo
#     # ... (POST) --> thêm 1 bản thảo mới
#     # detail --> xem chi tiết bản thảo
#     # .... (PUT) --> cập nhật bản thảo
#     # ....(DELETE) --> xóa bản thảo
#     # Muốn tạo bản thảo thì phải ở trạng thái đăng nhập
#     permission_classes = [permissions.IsAuthenticated()]
#
#     # Tất cả người dùng có quyền xem danh sách bản thảo.
#     # Tuy nhiên các action khác (chi tiết bản thảo, sửa, ... thì bắt buộc phải đăng nhập mới vào xem được)
#
#     def get_permissions(self):
#         if self.action == 'list':
#             return [permissions.AllowAny()]
#         return [permissions.IsAuthenticated()]
#
#     @action(methods=['post'], detail=True, url_path='hide-tltk',
#             url_name='hide-tltk')
#     # /banthao/{pk}/hide-banthao
#     def hide_tltk(self, request, pk):
#         try:
#             d = TaiLieuThamKhao.objects.get(pk=pk)
#             d.active = False
#             d.save()
#         except TaiLieuThamKhao.DoesNotExist:
#             return Response(status=status.HTTP_400_BAD_REQUEST)
#         return Response(data=TLTKSerializer(d).data, status=status.HTTP_200_OK)


# def index(request):
#     return render(request, 'index.html', {})

class HomeView(TemplateView):
    template_name = 'home.html'


class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email']


class SiteRegisterView(FormView):
    template_name = 'dangky.html'
    form_class = RegisterForm

    # Xử lý dữ liệu từ filed. Sau khi get data, return về 1 trang web mà ta mong muốn và lưu xuống CSDL
    def form_valid(self, form):
        data = form.cleaned_data
        new_user = User.objects.create_user(
            username=data['username'],
            password=data['password1'],
            email=data['email']
        )
        url = f"{reverse('dangnhap')}?username={new_user.username}"
        from pprint import pprint;
        pprint(url)
        # Cho phép chuyển sang 1 trang khác
        return redirect(url)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['username'] = self.request.GET.get('username')
        return context


class SiteLoginView(LoginView):
    template_name = 'dangnhap.html'


class SiteUpdateProfileView(LoginRequiredMixin, UpdateView):
    template_name = 'profile.html'
    model = User
    fields = ['anhdaidien', 'first_name', 'last_name', 'email']
    success_url = '/hoso/'

    def get_object(self, queryset=None):
        return self.request.user


class SiteLogoutView(LogoutView):
    template_name = 'dangxuat.html'


# # -------------------- STEP 1 --------------------
# def TitleView(request):
#     form = RawTitleForm()
#     if request.method == 'POST':
#         form = RawTitleForm(request.POST)
#         if form.is_valid():
#             print(form.cleaned_data)
#             BanThao.objects.create(**form.cleaned_data)
#         else:
#             print(form.errors)
#     context = {
#         'form': form
#     }
#     return render(request, 'buoc1.html', context)
#
#
# # -------------------- STEP 2 --------------------
# def AuthorView(request):
#     form = RawAuthorForm()
#     if request.method == 'POST':
#         form = RawAuthorForm(request.POST)
#         if form.is_valid():
#             print(form.cleaned_data)
#             TacGia.objects.create(**form.cleaned_data)
#             return redirect('/dstacgia')
#         else:
#             return render(request, 'error.html')
#     context = {
#         'form': form
#     }
#     return render(request, 'buoc2.html', context)
#
#
# def AuthorList(request):
#     author_list = {'author_list': TacGia.objects.all()}
#     return render(request, 'dstacgia.html', author_list)
#
#
# # -------------------- STEP 3 --------------------
# def FileList(request):
#     files = TapTin.objects.all()
#     return render(request, 'dstaptin.html', {'files': files})
#
#
# def UploadFile(request):
#     if request.method == 'POST':
#         form = FileForm(request.POST, request.FILES)
#         if form.is_valid():
#             # file is saved
#             form.save()
#             return redirect('dstaptin')
#     else:
#         form = RawFileForm()
#     return render(request, 'buoc3.html', {'form': form})
#
#
# def DeleteFile(request, pk):
#     if request.method == 'POST':
#         files = TapTin.objects.get(pk=pk)
#         files.delete()
#     return redirect('dstaptin')
#
#
# # -------------------- STEP 4 --------------------
# # def KeywordView(request):
#     form = RawKeywordForm()
#     if request.method == 'POST':
#         form = RawKeywordForm(request.POST)
#         if form.is_valid():
#             # print(form.cleaned_data)
#             # # TuKhoa.objects.create(**form.cleaned_data)
#             TuKhoa.objects.create(**form.cleaned_data)
#             print(form.cleaned_data)
#             return redirect('/dstukhoa')
#         else:
#             return render(request, 'error.html')
#     context = {
#         'form': form
#     }
#     return render(request, 'buoc4.html', context)
#
#
# def KeywordList(request):
#     keyword_list = {'keyword_list': TuKhoa.objects.all()}
#     return render(request, 'dstukhoa.html', keyword_list)

class submission_banthao(TemplateView):
    template_name = 'submission_banthao.html'

class submission_tacgia(TemplateView):
    template_name = 'submission_tacgia.html'

class submission_taptin(TemplateView):
    template_name = 'submission_taptin.html'

class submission_tukhoa(TemplateView):
    template_name = 'submission_tukhoa.html'

class submission_tltk(TemplateView):
    template_name = 'submission_tltk.html'