from django.db import models
from django.contrib.auth.models import AbstractUser
from ckeditor.fields import RichTextField


# Create your models here.
# Quan hệ 1-n: ForeignKey()
# Quan hệ n-n: ManyToManyField()
# ---> sẽ tự sinh bảng trung gian, nếu muốn thay đổi tên bảng dùng thuộc tính related_name()
# ---> nếu muốn tự tạo bảng trung gian dùng thuộc tính throught
# Lưu ý: Nếu bảng trung gian có bất kỳ thuộc tính nào khóa thì khi đó, ta sẽ vẽ thành thực thể yếu riêng và dùng thuộc tính throught
from django.forms import ModelForm

class User(AbstractUser):
    anhdaidien = models.ImageField(upload_to='uploads/%Y/%m', null=True)
    sex = [
        ('male', 'Nam'),
        ('female', 'Nữ'),
    ]
    gioitinh = models.CharField(max_length=10, null=True, choices=sex, default='male')

